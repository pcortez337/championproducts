﻿CREATE TABLE [dbo].[InvoiceDetail] (
    [InvoiceDetailId] BIGINT          IDENTITY (1, 1) NOT NULL,
    [InvoiceId]       BIGINT          NOT NULL,
    [ProductId]       BIGINT          NOT NULL,
    [Amount]          DECIMAL (18, 2) NOT NULL,
    [CreatedDate]     DATETIME        CONSTRAINT [DF_InvoiceDetail_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]     DATETIME        NULL,
    CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED ([InvoiceDetailId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_InvoiceDetail_InvoiceId]
    ON [dbo].[InvoiceDetail]([InvoiceId] ASC);

