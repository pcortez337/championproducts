﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [CustomerTypeId] INT           NOT NULL,
    [BusinessName]   VARCHAR (200) NULL,
    [LastName]       VARCHAR (50)  NOT NULL,
    [FirstName]      VARCHAR (50)  NOT NULL,
    [AddressId]      INT           NOT NULL,
    [CreatedDate]    DATETIME      CONSTRAINT [DF_Customer_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]      BIT           NOT NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Customer_LastNameFirstName]
    ON [dbo].[Customer]([LastName] ASC, [FirstName] ASC);

