﻿CREATE TABLE [dbo].[Product] (
    [ProductId]   INT          IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (50) NULL,
    [CreatedDate] DATETIME     CONSTRAINT [DF_Product_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]   BIT          NOT NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Product_Description]
    ON [dbo].[Product]([Description] ASC);

