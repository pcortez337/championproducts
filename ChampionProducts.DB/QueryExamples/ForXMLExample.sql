﻿-- Query several tables from sys.tables into an XML string
select TOP 10
       [object_id] as Id
      ,[name]
	  ,[Create_date]
  from sys.tables 
   FOR XML PATH('Table')